const request = require('request');
const TaskModel = require('../models/task_model');

const httpCodes = require('../commons/http_codes');
const messages = require('../../language/messages');

module.exports.create = function(req, res) {
  try {
    const task = new TaskModel(req.body);

    task.save()
      .then((created) => {
        res.status(httpCodes.CREATED).send(created);
      })
      .catch((err) => {
        if (err.name === 'ValidationError') {
          const errors = Object.keys(err.errors).map((errorField) => {
            return {error: err.errors[errorField].message};
          });
          res.status(httpCodes.BAD_REQUEST).send({errors: errors});
        } else {
          res.status(httpCodes.INTERNAL_SERVER_ERROR)
            .send({error: messages.internalServerError});
        }
      });

  } catch (e) {
    console.log(e);
    res.status(httpCodes.INTERNAL_SERVER_ERROR)
      .send({error: messages.internalServerError});
  }
};

module.exports.findAll = function(req, res) {
  try {
    TaskModel.find({}).sort({completed: 1}).sort({creationDate: 1})
      .then((tasks) => {
        if (tasks.length) {
          res.send(tasks);
        } else {
          res.status(httpCodes.NOT_FOUND)
            .send({error: messages.controller.task.notFound});
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(httpCodes.INTERNAL_SERVER_ERROR)
          .send({error: messages.internalServerError});
      });
  } catch (err) {
    console.log(err);
    res.status(httpCodes.INTERNAL_SERVER_ERROR)
      .send({error: messages.internalServerError});
  }
};

module.exports.remove = function(req, res) {
  try {
    TaskModel.findByIdAndRemove({_id: req.params.id})
      .then((task) => {
        if (task !== null) {
          res.status(httpCodes.GONE)
            .send({message: messages.controller.task.remove});
        } else {
          res.status(httpCodes.NOT_FOUND)
            .send({error: messages.controller.task.notFound});
        }
      })
      .catch((err) => {
        console.log(err);
        res.status(httpCodes.INTERNAL_SERVER_ERROR)
          .send({error: messages.internalServerError});
      });
  } catch (err) {
    console.log(err);
    res.status(httpCodes.INTERNAL_SERVER_ERROR)
      .send({error: messages.internalServerError});
  }
};

module.exports.update = function(req, res) {
  try {
    const task = new TaskModel(req.body);

    TaskModel.findByIdAndUpdate({ _id: task._id }, task)
      .then((task) => {
        if (task) {
          res.send({message: messages.controller.task.update});
        } else {
          res.status(httpCodes.NOT_FOUND)
            .send({error: messages.controller.task.notFound});
        }

      }).catch((err) => {
        console.log(err);
        res.status(httpCodes.INTERNAL_SERVER_ERROR)
          .send({error: messages.internalServerError});
      });
  } catch (err) {
    console.log(err);
    res.status(httpCodes.INTERNAL_SERVER_ERROR)
      .send({error: messages.internalServerError});
  }
};
