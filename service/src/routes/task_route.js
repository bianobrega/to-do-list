const taskController = require('../controllers/task_controller');

module.exports = function(app) {
  app.post('/api/task', taskController.create);
  app.put('/api/task/', taskController.update);
  app.get('/api/task', taskController.findAll);
  app.delete('/api/task/:id', taskController.remove);
}