const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TaksSchema = new Schema({
  title: {
    type: String,
    required: [true, 'O título da tarefa é obrigatório'],
    validate: {
      validator: (title) => title.length < 100,
      message: 'O título da tarefa deve ter no máximo 100 caracteres',
    }
  },
  completed: {
    type: Boolean,
    default: false
  },
  creationDate: {
    type: Date,
    default: Date.now
  }
})

const Task = mongoose.model('task', TaksSchema);

module.exports = Task;
