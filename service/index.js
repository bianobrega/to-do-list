const config = require ('./config/config.json');
const app = require ('./config/server');
const person = require('./src/routes/task_route')(app);

app.listen(config.server.port, function() {
  console.log('Servidor ON utilizando a porta:' + config.server.port);
});
