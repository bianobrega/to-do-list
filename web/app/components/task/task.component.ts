import { Component,
         AfterViewInit }      from '@angular/core';
import { Response }           from '@angular/http';
import { FormGroup,
         FormBuilder,
         FormArray,
         FormControl,
         Validators }         from '@angular/forms';
import { Router }             from '@angular/router';
import { ValidationService }  from '../../shared/forms/validation.service';
import { TaskService }        from '../../service/task.service';

import { Task }               from '../../model/task.model';

// external libraries references
declare var $: any;
declare var Materialize: any;

@Component({
    moduleId: module.id,
    providers: [
        TaskService,
    ],
    selector: 'task',
    templateUrl: 'task.component.html'
})
export class TaskComponent {

  tasks: [Task];

  taskForm: FormGroup;
  submitted = false;
  submitError = false;
  submitErrorMessages: string[];

  constructor(private fb: FormBuilder,
    private taskService: TaskService,
    private router: Router) { }

  ngOnInit() {
    // reset status
    this.submitErrorMessages = null;
    this.submitted = false;
    this.submitError = false;

    this.taskForm = this.fb.group({
      'title': [null,
        Validators.compose([
        Validators.required
        ])
      ]
    });

      this.getTasks();
    }

  save(formData: any) {
    var task: Task = new Task();
    task.title = formData.title;

    this.submitted = true;
    this.submitErrorMessages = [];

    this.taskService
      .save(task)
      .subscribe(
        (response: Response) => {
          this.submitted = false;
          if (response) {
            this.getTasks();
            Materialize.toast("Tarefa cadastrada com sucesso", 7000);
          }
          $('html, body').animate({ scrollTop: 0 }, 300);
        },

        (error: any) => {
          console.log(error);
          this.getTasks();
          this.submitted = false;
          if (JSON.parse(error['status']) !== 0) {
            var errors = ([].concat(JSON.parse(error['_body'])));
            this.submitErrorMessages = errors.map(function(elem: { 'error': string }) {
              return elem.error;
            });
            this.submitError = true;
          }
        });
  }

  delete(taskId:String) {
    this.taskService.delete(taskId)
      .subscribe(
        (response: Response) => {
         
        },
        (response: any) => {
          console.log(response);
          this.getTasks();
          if (JSON.parse(response['status']) == 410) {
            Materialize.toast(JSON.parse(response['_body']).message, 7000);
          }
          if (JSON.parse(response['status']) !== 0) {
            var errors = ([].concat(JSON.parse(response['_body'])));
            this.submitErrorMessages = errors.map(function(elem: { 'error': string }) {
              return elem.error;
            });
            this.submitError = true;
          }
        }
      );
  }

  concludeTask(task:Task) {
    task.completed = true;
    this.taskService.updateTask(task)
    .subscribe(
      (response: Response) => {
        this.submitted = false;
        if (response) {
          this.getTasks();
          Materialize.toast(JSON.parse(response['_body']).message, 7000);
        } 

        $('html, body').animate({ scrollTop: 0 }, 300);
      },
      (error: any) => {
        console.log(error);
        this.getTasks();
        if (JSON.parse(error['status']) !== 0) {
          var errors = ([].concat(JSON.parse(error['_body'])));
          this.submitErrorMessages = errors.map(function(elem: { 'error': string }) {
            return elem.error;
          });
          this.submitError = true;
        }
      }
      );
  }

  private getTasks() {
    this.taskService.getTasks()
    .subscribe(
      (tasks: [Task]) => {
        this.tasks = tasks;
        console.log(tasks)
      },
      (error) => {
        console.log(error);
        this.tasks = null;
        if (JSON.parse(error['status']) !== 404) {
          var errors:any[];
          if (JSON.parse(error['status']) !== 0) {
            var errors = ([].concat(JSON.parse(error['_body'])));
          } else {
            errors = ([].concat({error: "Estamos com problemas no momento. Tente novamente mais tarde."}));
          }
          this.submitErrorMessages = errors.map(function(elem: { 'error': string }) {
              return elem.error;
            });
          this.submitError = true;
        }
      }
    );
  };
}
