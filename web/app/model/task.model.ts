export class Task {
  constructor() { }

  public _id: string;
  public title: string;
  public creationDate: string;
  public completed: boolean = false;
}
