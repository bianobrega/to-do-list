import { FormGroup } from '@angular/forms';
/*
* ValidationService
* Provides a set of validation functions and, if an input is not valid,
* returns a friendly message to the user.
* TO-DO: i18n
*/
export class ValidationService {
  static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
    let config = {
      'required': 'Obrigatório.',
    };

    return config[validatorName];
  }
}
