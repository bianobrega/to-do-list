import { Component,
         Input } from '@angular/core';

@Component({
    selector: 'general-error',
    template: `<div class="row" *ngIf="message !== null">
    <div class="col s12 m10 offset-m1 form-submit-error rounded-corners-10">
      <ul>
        <option *ngFor="let error of errors">
          {{error}}
        </option>
      </ul>
    </div>
  </div>`
})
export class GeneralErrorComponent {
    @Input() errors: string[];

    constructor() { }

}
