import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'dateBR'
})

export class DatePipe implements PipeTransform {
  transform(date: string): string {
     if (!date) {
       return '';
    }
    
    let valueDate = date.replace(/\D/g, '');
    let listDate = valueDate.match(/^(\d{4})(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})(\d{3})$/);
    
    date = listDate[3] + '/' + listDate[2] + '/' + listDate[1]
       
    return date;
  }
   
}