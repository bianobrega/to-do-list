import { Component } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'default-footer',
    templateUrl: 'footer.template.html'
})
export class DefaultFooterComponent {
    constructor() { }
}
