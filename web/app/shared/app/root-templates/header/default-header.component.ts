import { Component }    from '@angular/core';
import { Router }       from '@angular/router';

@Component({
    moduleId: module.id,
    selector: 'default-header',
    templateUrl: 'default-header.template.html',

})
export class DefaultHeaderComponent {
  
    constructor(
      private router: Router) { }

}
