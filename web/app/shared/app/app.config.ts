import { OpaqueToken } from '@angular/core';

export interface AppConfig {
  apiEndpoints: {'task': string};
  title: string;
}

export const TO_DO_LIST_CONFIG: AppConfig = {
  apiEndpoints: {
    'task': 'http://localhost:9000/api/task/',
  },
  title: 'To-do List'
};
export let APP_CONFIG = new OpaqueToken('app.config');
