import { Routes }                     from '@angular/router';
import { PageNotFoundComponent }      from '../../components/page-not-found/page-not-found.component';
import { TaskComponent }            from '../../components/task/task.component';

export const APP_ROUTES: Routes = [
    { path: 'tarefas', component: TaskComponent, data: { title: 'Tarefas' } },
    { path: '', component: TaskComponent, data: { title: 'Tarefas' } },
    { path: '**', component: PageNotFoundComponent }
];
