import { Component,
         AfterViewInit } from '@angular/core';
import { Router }        from '@angular/router';

// A reference to jQuery
declare var $: any;

@Component({
    moduleId: module.id,
    selector: 'my-app',
    templateUrl: 'root-templates/main/app-general-template.html'
})
export class AppComponent implements AfterViewInit {

    constructor(private router: Router) {
    }

    ngAfterViewInit() {
        // Init jQuery plugins after the view is loaded
        $('.button-collapse').sideNav();
        $('.nav-wrapper ul a, #internal-menu ul a').unbind("click").click(function(evt: any){
          $('.button-collapse').sideNav('hide');
        });
        $('select').material_select();
        $('.carousel.carousel-slider').carousel(
            { full_width: true, indicators: true }
        );
    }
}
