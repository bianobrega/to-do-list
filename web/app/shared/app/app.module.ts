import { NgModule }                   from '@angular/core';
import { BrowserModule }              from '@angular/platform-browser';
import { FormsModule,
         ReactiveFormsModule }        from '@angular/forms';
import { RouterModule, Routes }       from '@angular/router';
import { AppComponent }               from './app.component';
import { HttpModule }                 from '@angular/http';

// Application components
import { PageNotFoundComponent }      from '../../components/page-not-found/page-not-found.component';
import { TaskComponent }              from '../../components/task/task.component';
import { DefaultFooterComponent }     from './root-templates/footer/default-footer.component';
import { DefaultHeaderComponent }     from './root-templates/header/default-header.component';

// Pipes
import { DatePipe }                   from '../pipes/date-pipe';

// Component that PageNotFoundComponent a validation error to a friendly user message
import { ControlMessagesComponent }   from '../forms/control-messages.component';
import { GeneralErrorComponent }      from '../forms/general-error.component';
// Set of validation functions and it's error messages
import { ValidationService }          from '../forms/validation.service';
// Service to get user information data
import { APP_CONFIG, TO_DO_LIST_CONFIG }  from './app.config';

import { Logger }                     from './logger.service';

// Routes
import { APP_ROUTES }                 from './app-routes.config';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule.forRoot(APP_ROUTES),
  ],
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    ControlMessagesComponent,
    GeneralErrorComponent,
    TaskComponent,
    DefaultFooterComponent,
    DefaultHeaderComponent,
    DatePipe, 
  ],
  providers: [
    ValidationService,
    Logger,
    { provide: APP_CONFIG, useValue: TO_DO_LIST_CONFIG }
  ],
  exports: [
    DatePipe,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
