import { Injectable }            from '@angular/core';
import { Headers,
         Http,
         Response,
         RequestOptions }        from '@angular/http';
import { Observable }            from 'rxjs';
import { TO_DO_LIST_CONFIG }     from '../shared/app/app.config';
import { Task }                  from '../model/task.model';

import 'rxjs/add/operator/map'

@Injectable()
export class TaskService {
    constructor(private http: Http) { }

    save(task: Task): Observable<Response> {
      return this.http
        .post(TO_DO_LIST_CONFIG.apiEndpoints.task, task)
          .map((response: Response) => {
            return response;
          });

    };  

    getTasks(): Observable<[Task]> {
      return this.http.get(TO_DO_LIST_CONFIG.apiEndpoints.task)
        .map((response: Response) => {
          return response.json();
        });
    }

    delete(taskId: String): Observable<Response> {
      return this.http
        .delete(TO_DO_LIST_CONFIG.apiEndpoints.task + taskId)
          .map((response: Response) => {
            return response;
          });
    };

    updateTask(task: Task): Observable<Response> {
      return this.http
        .put(TO_DO_LIST_CONFIG.apiEndpoints.task, task)
          .map((response: Response) => {
            return response;
          });
    };
}
