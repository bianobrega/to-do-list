import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './shared/app/app.module';

platformBrowserDynamic().bootstrapModule(AppModule)
  .then(success => console.log(`To-do List application - Bootstrap success`))
  .catch(error => console.log(error));
